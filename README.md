A simple version of Dynamo-style key-value storage

The implementation consists of three main pieces
1) Partitioning
2) Replication
3) Failure handling

The implementation provides both availability and linearizability at the same time. i.e it always perform read and write operations successfully even under failures. At the same time, a read operation should always return the most recent value. 

