package edu.buffalo.cse.cse486586.simpledynamo;

import android.database.Cursor;
import android.text.GetChars;
import android.util.Log;

public class CursorBuilder {

	private Cursor cursor;
	
	public CursorBuilder(){
		
	}
	
	public void setCursor(Cursor cursor){
		this.cursor = cursor;
	}
	
	public Cursor getCursor() {
		return cursor;
	}
}
