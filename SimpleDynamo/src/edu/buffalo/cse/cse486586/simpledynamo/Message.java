package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.util.Log;

public class Message implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8129839288767046761L;
	public int type;
	public String key;
	public String value;
	private String port;
	private Map<String,String> reply = null;
	private Cursor cursor;
	
	public Message(){
		
	}
	
	public void setType(int type){
		this.type = type;
	}
	
	public void setKey(String key){
		this.key = key;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public void setPort(String port){
		this.port = port;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(type);
		sb.append("||");
		sb.append(key);
		
		return sb.toString();
	}

	public int getType() {
		// TODO Auto-generated method stub
		return type;
	}

	public String getKey(){
		return this.key;
	}

	public String getValue(){
		return this.value;
	}

	public String getPort() {
		// TODO Auto-generated method stub
		return this.port;
	}

	public void setCursor(Cursor cursor) {
		// TODO Auto-generated method stub
		Map<String,String> result = new HashMap<String,String>();
		
		Log.v("Message","Returning Cursor");
		if(cursor!=null){
			cursor.moveToFirst();
		}

		for(int i = 0; i < cursor.getCount(); i++){
			int keyIndex = cursor.getColumnIndex(SimpleDynamoProvider.KEY_FIELD);
			int valIndex = cursor.getColumnIndex(SimpleDynamoProvider.VALUE_FIELD);
			int versionIndex = cursor.getColumnIndex(SimpleDynamoProvider.VERSION);
			if (keyIndex == -1 || valIndex == -1) {
				continue;
			}
			String key = cursor.getString(keyIndex);
			String val = cursor.getString(valIndex);
			int version = cursor.getInt(versionIndex);
			result.put(key, val+"||"+version);
			cursor.moveToNext();
		}
		this.reply = result;
	}
	
	public Cursor getCursor() {
		// TODO Auto-generated method stub
		if(reply.isEmpty() || reply == null){
			return null;
		}else{
			Iterator<String> keyIt = reply.keySet().iterator();
			String[] columnNames = new String[3];
			columnNames[0] = SimpleDynamoProvider.KEY_FIELD;
			columnNames[1] = SimpleDynamoProvider.VALUE_FIELD;
			columnNames[2] = SimpleDynamoProvider.VERSION;

			MatrixCursor mx = new MatrixCursor(columnNames);
			while(keyIt.hasNext()){
				String key = keyIt.next();
				String result = reply.get(key);
				String[] entries = result.split("\\|\\|");
 				mx.addRow(new Object[]{key,entries[0],Integer.parseInt(entries[1])});
			}
			return mx;
		}
		
		
	}

}
