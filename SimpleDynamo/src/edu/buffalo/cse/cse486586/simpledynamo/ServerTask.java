package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import android.util.Log;

public class ServerTask implements Runnable {

	private static final int SERVER_PORT = 10000;

	@Override
	public void run() {
		// TODO Auto-generated method stub
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(SERVER_PORT);
			while (true) {
				ObjectInputStream ois = null;
				try {
					Socket client = serverSocket.accept();
					BufferedInputStream bis = new BufferedInputStream(client.getInputStream());
					ois = new ObjectInputStream(bis);
					Message msg = (Message)ois.readObject();
					Log.v("Message","Message Received"+msg.type);
					new Thread(new MessageProcessor(msg)).start();
				} catch (IOException ex) {
					Log.v("Server Connection", "Coonection Accept Failed");
				} catch (ClassNotFoundException ex) {
					Log.v("Server Connection", "Class Not Found Failed");
				} finally {
					try {
						if (ois != null) {
							ois.close();
						}
					} catch (IOException ex) {
						Log.v("Server Connection", "Reader Close Failed");
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (serverSocket != null) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}