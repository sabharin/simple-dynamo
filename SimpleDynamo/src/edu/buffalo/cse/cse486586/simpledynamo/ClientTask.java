package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;


public class ClientTask implements Runnable{

	private String port;
	private Message msg;
	public ClientTask(String port,Message msg){
		this.port = port;
		this.msg = msg;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
        Socket socket;
		try {
			if(msg.type == 3){
				Log.v("Query Reply","Reply Part");
			}
			socket = new Socket(InetAddress.getByAddress(new byte[] {
			        10, 0, 2, 2
			}), Integer.parseInt(port));

			BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(this.msg);
            if(msg.type == 3){
				Log.v("Query Reply","Write Completed");
			}
            oos.flush();
            bos.flush();
            oos.close();
            bos.close();
            socket.close();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			Log.v("Client Task","Number Format Exception");
			e.printStackTrace();
		} catch (UnknownHostException e) {
			Log.v("Client Task","Unknown Host Exception");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.v("Client Task","IO Exception");
			e.printStackTrace();
		} catch(Exception e){
			String message = e.getMessage();
			Log.v("Client Task",message);
		}
	}    	
}