package edu.buffalo.cse.cse486586.simpledynamo;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.util.Log;

public class KeyCursorBuilder {
	private MatrixCursor cursor;
	
	public KeyCursorBuilder() {
		// TODO Auto-generated constructor stub
	}
	
	public void addCursor(Cursor resultCursor){
		if(cursor!=null){
			this.cursor.moveToLast();
			int currentVersionIndex = this.cursor.getColumnIndex(SimpleDynamoProvider.VERSION);
			int currentVersion = this.cursor.getInt(currentVersionIndex);
			if (resultCursor != null) {
				resultCursor.moveToFirst();
				int newVersion = resultCursor.getInt(currentVersionIndex);
				if(currentVersion < newVersion){
					this.cursor = new MatrixCursor(new String[]{SimpleDynamoProvider.KEY_FIELD,SimpleDynamoProvider.VALUE_FIELD,SimpleDynamoProvider.VERSION});;
					
					for (int i = 0; i < resultCursor.getCount(); i++) {
						Log.v("Key Dump Linearizablity","Iterating Result Cursor"+i);
						int keyIndex = resultCursor
								.getColumnIndex(SimpleDynamoProvider.KEY_FIELD);
						int valIndex = resultCursor
								.getColumnIndex(SimpleDynamoProvider.VALUE_FIELD);
						int versionIndex = resultCursor.getColumnIndex(SimpleDynamoProvider.VERSION);
						if (keyIndex == -1 || valIndex == -1 || versionIndex == -1) {
							continue;
						}
						String key = resultCursor.getString(keyIndex);
						String val = resultCursor.getString(valIndex);
						int version = resultCursor.getInt(versionIndex);
						this.cursor.addRow(new Object[] { key, val, version});
						resultCursor.moveToNext();
					}
				}
			}
		}else {
			this.cursor = new MatrixCursor(new String[]{SimpleDynamoProvider.KEY_FIELD,SimpleDynamoProvider.VALUE_FIELD,SimpleDynamoProvider.VERSION});;
			if (resultCursor != null) {
				resultCursor.moveToFirst();
				for (int i = 0; i < resultCursor.getCount(); i++) {
					Log.v("Key Dump Linearizablity", "Iterating Result Cursor"
							+ i);
					int keyIndex = resultCursor
							.getColumnIndex(SimpleDynamoProvider.KEY_FIELD);
					int valIndex = resultCursor
							.getColumnIndex(SimpleDynamoProvider.VALUE_FIELD);
					int versionIndex = resultCursor
							.getColumnIndex(SimpleDynamoProvider.VERSION);
					if (keyIndex == -1 || valIndex == -1 || versionIndex == -1) {
						continue;
					}
					String key = resultCursor.getString(keyIndex);
					String val = resultCursor.getString(valIndex);
					String version = resultCursor.getString(versionIndex);
					this.cursor.addRow(new String[] { key, val, version });
					resultCursor.moveToNext();
				}
			}
		}
		Log.v("Local Dump Key","Result Checked");
	}
	
	public Cursor getCursor() {
		return cursor;
	}

}
