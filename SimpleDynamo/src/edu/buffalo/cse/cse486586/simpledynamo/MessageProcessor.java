package edu.buffalo.cse.cse486586.simpledynamo;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class MessageProcessor implements Runnable {

	private Message msg;
	
	public MessageProcessor(Message msg){
		this.msg = msg;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		switch(msg.getType()){
		case 0:
	    	ContentValues cv = new ContentValues();
	    	cv.put(SimpleDynamoProvider.KEY_FIELD, msg.getKey());
	    	cv.put(SimpleDynamoProvider.VALUE_FIELD, msg.getValue());
			Uri.Builder uriBuilder = new Uri.Builder();
			uriBuilder.authority("");
			uriBuilder.scheme("");
			
			SimpleDynamoProvider.context.getContentResolver().insert(SimpleDynamoProvider.dummyUri, cv);
			
			Message insertReplyMsg = new Message();
			insertReplyMsg.setPort(SimpleDynamoProvider.myPort);
			insertReplyMsg.setType(5);
			insertReplyMsg.setKey(msg.getKey());
			Log.v("Message Reply","Reply for query");
			new ClientTask(msg.getPort(), insertReplyMsg).run();

			break;
		case 1:
			String senderPort = msg.getPort();
			Cursor cursor = SimpleDynamoProvider.context.getContentResolver().query(SimpleDynamoProvider.dummyUri,null, msg.getKey(), null, null, null);
			Message replyMsg = new Message();
			replyMsg.setPort(SimpleDynamoProvider.myPort);
			replyMsg.setCursor(cursor);
			replyMsg.setKey(msg.key);
			replyMsg.setType(3);
			Log.v("Message Reply","Reply for query "+msg.key+"Size::"+cursor.getCount());
			new ClientTask(senderPort, replyMsg).run();
			// Query
			break;
		case 2:
			SimpleDynamoProvider.context.getContentResolver().delete(SimpleDynamoProvider.dummyUri,msg.getKey(),null);
			Message deleteReplyMsg = new Message();
			deleteReplyMsg.setPort(SimpleDynamoProvider.myPort);
			deleteReplyMsg.setType(4);
			Log.v("Message Reply","Reply for delete "+msg.key);
			new ClientTask(msg.getPort(), deleteReplyMsg).run();

			Log.v("Message Reply","Reply for delete");
			// Delete
			break;
		case 3:
			Cursor curs = msg.getCursor();
			SimpleDynamoProvider.queryLock.lock();
			SimpleDynamoProvider.cs.setCursor(curs);
			SimpleDynamoProvider.queryCondition.signal();		
			Log.v("Message Processor","Releasing Query Wait "+msg.getPort()+":: key "+msg.key);
			SimpleDynamoProvider.queryLock.unlock();
			// Query Response
			break;
		case 4:
			SimpleDynamoProvider.deleteLock.lock();
			SimpleDynamoProvider.deleteCondition.signal();
			Log.v("Message Processor","Releasing Delete Wait "+msg.getPort());
			SimpleDynamoProvider.deleteLock.unlock();
			// Delete Response
			break;
		case 5:
			SimpleDynamoProvider.insertLock.lock();
			SimpleDynamoProvider.insertCondition.signal();
			Log.v("Message Processor","Releasing Insert Wait "+msg.getPort()+"Key::"+msg.getKey());
			SimpleDynamoProvider.insertLock.unlock();
			break;
		case 6:
	    	cv = new ContentValues();
	    	cv.put(SimpleDynamoProvider.KEY_FIELD, msg.getKey());
	    	cv.put(SimpleDynamoProvider.VALUE_FIELD, msg.getValue());
			uriBuilder = new Uri.Builder();
			uriBuilder.authority("");
			uriBuilder.scheme("");
			
			SimpleDynamoProvider.context.getContentResolver().insert(SimpleDynamoProvider.dummyUri, cv);
			
			insertReplyMsg = new Message();
			insertReplyMsg.setPort(SimpleDynamoProvider.myPort);
			insertReplyMsg.setType(7);
			insertReplyMsg.setKey(msg.getKey());
			Log.v("Message Reply","Reply for replica insert query");
			new ClientTask(msg.getPort(), insertReplyMsg).run();
			break;
		case 7:
			Log.v("Message Processor","Received Replica Reply "+msg.getPort()+"Key::"+msg.getKey());

			break;
			
		default:
			break;
		}
	}

}
