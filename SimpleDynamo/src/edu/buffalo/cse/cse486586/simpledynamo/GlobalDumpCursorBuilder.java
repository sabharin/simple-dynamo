package edu.buffalo.cse.cse486586.simpledynamo;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.util.Log;

public class GlobalDumpCursorBuilder {
	private MatrixCursor cursor;
	
	public GlobalDumpCursorBuilder(){
		
	}
	
	public void addCursor(Cursor resultCursor){
		if(cursor!=null){
			this.cursor.moveToLast();
		}else {
			this.cursor = new MatrixCursor(new String[]{SimpleDynamoProvider.KEY_FIELD,SimpleDynamoProvider.VALUE_FIELD});
		}
		if (resultCursor != null) {
			resultCursor.moveToFirst();
			for (int i = 0; i < resultCursor.getCount(); i++) {
				Log.v("Global Dump","Iterating Result Cursor"+i);
				int keyIndex = resultCursor
						.getColumnIndex(SimpleDynamoProvider.KEY_FIELD);
				int valIndex = resultCursor
						.getColumnIndex(SimpleDynamoProvider.VALUE_FIELD);
				if (keyIndex == -1 || valIndex == -1) {
					continue;
				}
				String key = resultCursor.getString(keyIndex);
				String val = resultCursor.getString(valIndex);
				this.cursor.addRow(new String[] { key, val });
				resultCursor.moveToNext();
			}
		}

		Log.v("Global Dump","Result Added");
	}
	
	public Cursor getCursor() {
		return cursor;
	}

}
