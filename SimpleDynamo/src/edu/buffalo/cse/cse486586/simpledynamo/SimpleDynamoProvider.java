package edu.buffalo.cse.cse486586.simpledynamo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.w3c.dom.NodeList;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;

public class SimpleDynamoProvider extends ContentProvider {

	public static final Uri contentUri = Uri
			.parse("content://edu.buffalo.cse.cse486586.simpledynamo.provider");

	public static final Uri dummyUri = Uri
			.parse("content://edu.buffalo.cse.cse486586.simpledynamo.provider/dummy");

	private MessengerDBHelper dbHelper;

	static final String KEY_FIELD = "key";
	static final String VALUE_FIELD = "value";

	private static final String DBNAME = "SIMPLE_DYNAMO";

	private SQLiteDatabase sqliteDatabase = null;

	static Lock insertLock = new ReentrantLock();
	static Condition insertCondition = insertLock.newCondition();
	static Lock queryLock = new ReentrantLock();
	static Condition queryCondition = queryLock.newCondition();
	static Lock deleteLock = new ReentrantLock();
	static Condition deleteCondition = deleteLock.newCondition();
	static String waitObj = new String("waitObj");

	private String portStr;
	
	static CursorBuilder cs = new CursorBuilder();
	
	static DeleteBuilder db = new DeleteBuilder();
	
	private TreeMap<String,String> listMap = null;

	static String myPort = null;

	static Context context = null;

	public static String VERSION = "version";

	
	@Override
	public String getType(Uri uri) {
		// You do not need to implement this.
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		String key = values.get(KEY_FIELD).toString();
		String value = values.get(VALUE_FIELD).toString();
		
		StringBuilder conditionSb = new StringBuilder(
				MessengerDBHelper.KEY_COLUMN);
		conditionSb.append("=?");
		String args[] = new String[1];
		args[0] = key;

		String hash = null;
		try {
			hash = genHash(key);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		if (!uri.toString().equals(dummyUri.toString())) {
				List<String> nodeList = getNodeList(hash,key);
			
				if (nodeList.get(0).equalsIgnoreCase(portStr)) {
					
					sqliteDatabase = dbHelper.getWritableDatabase();
					Log.v("Query Insert", values.toString());
					Cursor cursor = sqliteDatabase.query(
							MessengerDBHelper.TABLENAME, null,
							conditionSb.toString(), args, null, null, null);
					int version = 0;

					if(cursor != null){
						cursor.moveToFirst();
						int colIndex = cursor.getColumnIndex(SimpleDynamoProvider.VERSION);
						version = cursor.getInt(colIndex);
						version++;

					}
					values.put(SimpleDynamoProvider.VERSION, version);
					if(version == 0){
						sqliteDatabase.insert(MessengerDBHelper.TABLENAME, null,
							values);
					}else{
						sqliteDatabase.replace(MessengerDBHelper.TABLENAME, null,
								values);
					}
					
					nodeList.remove(0);
					
					boolean notify = false;
					Iterator<String> portIt = nodeList.iterator();
					Message msg = new Message();
					msg.setPort(myPort);
					msg.setKey(key);
					msg.setValue(value);
					msg.setType(6);

					while (portIt.hasNext() && !notify) {
						String port = portIt.next();
						new Thread(new ClientTask(port, msg)).start();

						// Send Data to Port
/*						synchronized (SimpleDynamoProvider.waitObj) {
							insertLock.lock();
							try {
								new Thread(new ClientTask(port, msg)).start();
								insertCondition.await(2L,TimeUnit.SECONDS);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							insertLock.unlock();
						}
*/					}
					
				} else {
					Iterator<String> portIt = nodeList.iterator();
					String partition = nodeList.get(0);
					boolean notify = false;
					Message msg = new Message();
					msg.setPort(myPort);
					msg.setKey(key);
					msg.setValue(value);
					msg.setType(0);

					while (portIt.hasNext() && !notify) {
						String port = portIt.next();
						// Send Data to Port
						synchronized (SimpleDynamoProvider.waitObj) {
							insertLock.lock();
							try {
								Log.v("Insert","Sending Insert for "+key+" to "+port);
								new Thread(new ClientTask(port, msg)).start();
								notify = insertCondition.await(2L,TimeUnit.SECONDS);
								Log.v("Insert","Sending Insert for "+key+" to "+port + "Notified::"+notify);
								if(!(port.equalsIgnoreCase(partition))){
									notify = false;
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							insertLock.unlock();
						}
					}
				}
				// Send insert Message to port to Send
		} else{
			sqliteDatabase = dbHelper.getWritableDatabase();
			Log.v("Query Insert Else", values.toString());
			
			Cursor cursor = sqliteDatabase.query(
					MessengerDBHelper.TABLENAME, null,
					conditionSb.toString(), args, null, null, null);
			
			int version = 0;
			
			if(cursor != null && cursor.getCount() != 0){
				
				cursor.moveToFirst();
				int colIndex = cursor.getColumnIndex(SimpleDynamoProvider.VERSION);
				Log.v("Cursor Location",colIndex+"");
				version = cursor.getInt(colIndex);
				version++;
			}
			values.put(SimpleDynamoProvider.VERSION, version);
			
			if(version == 0){
				sqliteDatabase.insert(MessengerDBHelper.TABLENAME, null,
					values);
			}else {
				sqliteDatabase.replace(MessengerDBHelper.TABLENAME, null,
						values);
			}

			List<String> nodeList = getNodeList(hash,key);
			if(nodeList.get(0).equalsIgnoreCase(myPort)){
				// Send to other nodes
				Message msg = new Message();
				msg.setPort(myPort);
				msg.setKey(key);
				msg.setValue(value);
				msg.setType(6);

				nodeList.remove(0);
				
				boolean notify = false;
				Iterator<String> portIt = nodeList.iterator();
				while (portIt.hasNext() && !notify) {
					String port = portIt.next();
					new Thread(new ClientTask(port, msg)).start();

					// Send Data to Port
/*					synchronized (SimpleDynamoProvider.waitObj) {
						insertLock.lock();
						try {
							new Thread(new ClientTask(port, msg)).start();
							insertCondition.await(2L,TimeUnit.SECONDS);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						insertLock.unlock();
					}
*/				}
			}
		}
		return uri;
	}

	private List<String> getNodeList(String hash, String key) {
		// TODO Auto-generated method stub
		Iterator<String> listIt = listMap.keySet().iterator();
		List<String> portToSend = new ArrayList<String>();
		while (listIt.hasNext()) {
			String nodeHash = listIt.next();
			if (nodeHash.compareTo(hash) > 0) {
				if (portToSend.size() < 3) {
					portToSend.add(listMap.get(nodeHash));
				} else {
					break;
				}
			}
		}

		if (portToSend.size() < 3) {
			listIt = listMap.keySet().iterator();
			while (listIt.hasNext()) {
				String nodeHash = listIt.next();
				if (portToSend.size() < 3) {
					portToSend.add(listMap.get(nodeHash));
				} else {
					break;
				}
			}
		}
		
		Log.v("Ports","Key::"+key+"Hash::"+hash+portToSend.toString());

		return portToSend;
	}

	@Override
	public boolean onCreate() {
		// If you need to perform any one-time initialization task, please do it
		// here.
		Log.v("OnCreate", "OnCreate in Provider");
		Log.v("On Create","Waiting for WaitObj");
		synchronized (SimpleDynamoProvider.waitObj) {
			Log.v("OnCreate","Wait Obj Lock obtained");
			context = getContext();
			TelephonyManager tel = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			this.portStr = tel.getLine1Number().substring(
					tel.getLine1Number().length() - 4);
			this.myPort = String.valueOf((Integer.parseInt(portStr) * 2));
			dbHelper = new MessengerDBHelper(getContext(), DBNAME, null, 1);

			listMap = new TreeMap<String, String>();

			new Thread(new ServerTask()).start();

			try {
				listMap.put(genHash("5554"), "11108");
				listMap.put(genHash("5556"), "11112");
				listMap.put(genHash("5558"), "11116");
				listMap.put(genHash("5560"), "11120");
				listMap.put(genHash("5562"), "11124");

				Iterator<String> keyIt = listMap.keySet().iterator();
				String prev = null;
				String next = null;
				boolean foundPort = false;
				while (keyIt.hasNext()) {
					String port = keyIt.next();
					if (!foundPort) {
						if (listMap.get(port).equalsIgnoreCase(myPort)) {
							foundPort = true;
						} else {
							prev = port;
						}
					} else {
						next = port;
					}
				}

				if (prev == null) {
					prev = listMap.lastKey();
				}

				if (next == null) {
					next = listMap.firstKey();
				}

				Log.v("On Create","Previous and Next obtained");
				Message msg = new Message();
				msg.setPort(myPort);
				msg.setKey("@");
				msg.setType(1);

				Cursor cursor = null;

				keyIt = listMap.keySet().iterator();

				Log.v("On Create","Waiting for writable database obtained");
				sqliteDatabase = dbHelper.getWritableDatabase();
				Log.v("On Create","Writable Database obtained");
				Log.v("On Create","Key it has next"+keyIt.hasNext());
				while (keyIt.hasNext()) {
					String port = keyIt.next();
					if (!(port.equalsIgnoreCase(myPort))) {
						queryLock.lock();
						new Thread(new ClientTask(listMap.get(port), msg))
								.start();
						queryCondition.await(100L, TimeUnit.MILLISECONDS);
						cursor = cs.getCursor();
						queryLock.unlock();

						if (cursor != null) {
							int keyIndex = cursor
									.getColumnIndex(SimpleDynamoProvider.KEY_FIELD);
							int valueIndex = cursor
									.getColumnIndex(SimpleDynamoProvider.VALUE_FIELD);
							int versionIndex = cursor
									.getColumnIndex(SimpleDynamoProvider.VERSION);

							cursor.moveToFirst();

							for (int i = 0; i < cursor.getCount(); i++) {
								String key = cursor.getString(keyIndex);
								String value = cursor.getString(valueIndex);
								List<String> nodeList = getNodeList(
										genHash(key), key);
								if (nodeList.get(0).equals(myPort)
										|| nodeList.get(1).equals(myPort)
										|| nodeList.get(2).equals(myPort)) {
									int version = cursor.getInt(versionIndex);

									ContentValues cv = new ContentValues();
									cv.put(KEY_FIELD, key);
									cv.put(VALUE_FIELD, value);
									cv.put(VERSION, version);
									Log.v("On Create ", key + "||" + value);
									sqliteDatabase.replace(
											MessengerDBHelper.TABLENAME, null,
											cv);

								}

								cursor.moveToNext();
							}
						}
					}
				}

				/*
				 * queryLock.lock(); new Thread(new ClientTask(next,
				 * msg)).start(); queryCondition.await(1L,TimeUnit.SECONDS);
				 * cursor = cs.getCursor(); queryLock.unlock();
				 * 
				 * if(cursor!=null){ cursor.moveToFirst(); for(int
				 * i=0;i<cursor.getCount();i++){ String key =
				 * cursor.getString(keyIndex); String value =
				 * cursor.getString(valueIndex); List<String> nodeList =
				 * getNodeList(genHash(key), key); if
				 * (nodeList.get(0).equals(myPort) ||
				 * nodeList.get(1).equals(myPort) ||
				 * nodeList.get(2).equals(myPort)) { int version =
				 * cursor.getInt(versionIndex);
				 * 
				 * ContentValues cv = new ContentValues(); cv.put(KEY_FIELD,
				 * key); cv.put(VALUE_FIELD,value); cv.put(VERSION, version);
				 * 
				 * sqliteDatabase.replace(MessengerDBHelper.TABLENAME, null,
				 * cv);
				 * 
				 * }
				 * 
				 * cursor.moveToNext(); } }
				 */
			} catch (Exception e) {
				e.printStackTrace();
			}
			// Get Messages for first and last
			// Iterate
			// Add to the db
		} 
		Log.v("On Create","Returning True");
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		StringBuilder conditionSb = new StringBuilder(
				MessengerDBHelper.KEY_COLUMN);
		conditionSb.append("=?");
		String args[] = new String[1];
		args[0] = selection;

		String hash = null;
		try {
			hash = genHash(selection);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Cursor cursor = null;

		if (selection.equals("@")) {
			Log.v("Query @","Querying local repo");
			sqliteDatabase = dbHelper.getReadableDatabase();
			cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,
					projection, null, null, null, null, null);
			Log.v("Get Count of cursor Local Dump",cursor.getCount()+"");
			if (uri.toString().equals(dummyUri.toString())){
				return cursor;
			}
		} else if (selection.equals("*")) {
			sqliteDatabase = dbHelper.getReadableDatabase();
			cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,
					projection, null, null, null, null, null);
			Log.v("Local * query","Count is "+ cursor.getCount());
			GlobalDumpCursorBuilder gdcs = new GlobalDumpCursorBuilder();
			gdcs.addCursor(cursor);
			Iterator<String> portIt = listMap.keySet().iterator();
			Log.v("Global Dump","List"+listMap.keySet());
			while (portIt.hasNext()) {
				Log.v("Global Dump","Iterating to list");
				String port = portIt.next();
				if (!(port.equalsIgnoreCase(myPort))) {
					Message msg = new Message();
					msg.setPort(myPort);
					msg.setKey("@");
					msg.setType(1);
					// Send Data to Port

					synchronized(SimpleDynamoProvider.waitObj){
						queryLock.lock();
						new Thread(new ClientTask(listMap.get(port), msg)).start();
						Log.v("Query","Locking on port"+listMap.get(port));
						try {
							queryCondition.await(2L,TimeUnit.SECONDS);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						cursor = cs.getCursor();
						gdcs.addCursor(cursor);
						queryLock.unlock();
					}

					// Wait for 100 ms
				}
			}
			cursor = gdcs.getCursor();
			// Send msg to all nodes
		} else {
			if (uri.toString().equals(dummyUri.toString())){
				List<String> nodeList = getNodeList(hash,selection);
				args[0] = selection;
				if (nodeList.get(0).equalsIgnoreCase(myPort)) {
					sqliteDatabase = dbHelper.getWritableDatabase();
					Log.v("Query Dummy", selection);
					cursor = sqliteDatabase.query(
							MessengerDBHelper.TABLENAME, null,
							conditionSb.toString(), args, null, null, null);
/*					KeyCursorBuilder kb = new KeyCursorBuilder();
					kb.addCursor(cursor);
					nodeList.remove(0);
					Iterator<String> portIt = nodeList.iterator();
					while (portIt.hasNext()) {
						String port = portIt.next();
						Message msg = new Message();
						msg.setPort(myPort);
						msg.setKey(selection);
						msg.setType(1);
						// Send Data to Port

						synchronized (SimpleDynamoProvider.waitObj) {
							queryLock.lock();
							try {
								Log.v("Query", "Locking on port" + port + selection);
								new Thread(new ClientTask(port, msg)).start();
								queryCondition.await(2L,
										TimeUnit.SECONDS);
						//		Log.v("Query", "Locking on port" + port + selection + "Released "+ notify);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							cursor = cs.getCursor();
							kb.addCursor(cursor);
							queryLock.unlock();
						}
					}
					cursor = kb.getCursor();
*/				}else{
					sqliteDatabase = dbHelper.getWritableDatabase();
					cursor = sqliteDatabase.query(MessengerDBHelper.TABLENAME,null,conditionSb.toString(),args,null,null,null);
					Log.v("Query Dummy",selection +"::"+cursor.getCount());
				}
				return cursor;
			}else{
				List<String> nodeList = getNodeList(hash,selection);
				args[0] = selection;
				if (nodeList.get(0).equalsIgnoreCase(myPort)) {
					sqliteDatabase = dbHelper.getWritableDatabase();
					Log.v("Query", selection);
					cursor = sqliteDatabase.query(
							MessengerDBHelper.TABLENAME, null,
							conditionSb.toString(), args, null, null, null);
/*					KeyCursorBuilder kb = new KeyCursorBuilder();
					kb.addCursor(cursor);
					nodeList.remove(0);
					Iterator<String> portIt = nodeList.iterator();
					while (portIt.hasNext()) {
						String port = portIt.next();
						Message msg = new Message();
						msg.setPort(myPort);
						msg.setKey(selection);
						msg.setType(1);
						// Send Data to Port

						synchronized (SimpleDynamoProvider.waitObj) {
							queryLock.lock();
							try {
								Log.v("Query", "Locking on port" + port + selection);
								new Thread(new ClientTask(port, msg)).start();
								queryCondition.await(2L,
										TimeUnit.SECONDS);
						//		Log.v("Query", "Locking on port" + port + selection + "Released "+ notify);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							cursor = cs.getCursor();
							kb.addCursor(cursor);
							queryLock.unlock();
						}
					}
					cursor = kb.getCursor();
*/
				} else {
					Iterator<String> portIt = nodeList.iterator();
					String partition = nodeList.get(0);
					boolean notify = false;
					while (portIt.hasNext() && !notify) {
						String port = portIt.next();
						Message msg = new Message();
						msg.setPort(myPort);
						msg.setKey(selection);
						msg.setType(1);
						// Send Data to Port

						synchronized (SimpleDynamoProvider.waitObj) {
							queryLock.lock();
							try {
								Log.v("Query", "Locking on port" + port + selection);
								new Thread(new ClientTask(port, msg)).start();
								notify = queryCondition.await(2L,
										TimeUnit.SECONDS);
								if(!(port.equalsIgnoreCase(partition))){
									notify = false;
								}
								Log.v("Query", "Locking on port" + port + selection + "Released "+ notify);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							cursor = cs.getCursor();
							queryLock.unlock();
						}
					}
				}
			}
		}
		cursor = getKeyValue(cursor);
		return cursor;
	}

	private Cursor getKeyValue(Cursor cursor) {
		// TODO Auto-generated method stub
		
		if(cursor != null){
			MatrixCursor mx = new MatrixCursor(new String[]{SimpleDynamoProvider.KEY_FIELD,SimpleDynamoProvider.VALUE_FIELD});
			cursor.moveToFirst();
			int keyIndex = cursor.getColumnIndex(SimpleDynamoProvider.KEY_FIELD);
			int valueIndex = cursor.getColumnIndex(SimpleDynamoProvider.VALUE_FIELD);
			for(int i=0;i<cursor.getCount();i++){
				String key = cursor.getString(keyIndex);
				String value = cursor.getString(valueIndex);
				mx.addRow(new String[]{key,value});
				cursor.moveToNext();
			}
			return mx;
		}
		return null;
	}

	public int delete(Uri uri, String selection, String[] selectionArgs) {

		StringBuilder conditionSb = new StringBuilder(
				MessengerDBHelper.KEY_COLUMN);
		conditionSb.append("=?");
		String args[] = new String[1];
		args[0] = selection;
		
		String hash = null;
		try {
			hash = genHash(selection);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (selection.equals("@")) {

			sqliteDatabase = dbHelper.getWritableDatabase();
			sqliteDatabase.delete(MessengerDBHelper.TABLENAME,
					null, null);
		} else if (selection.equals("*")) {

			sqliteDatabase = dbHelper.getWritableDatabase();
			sqliteDatabase.delete(MessengerDBHelper.TABLENAME,
					null, null);
			
			Iterator<String> portIt = listMap.keySet().iterator();
			while (portIt.hasNext()) {
				Log.v("Global Dump","Iterating ot list");
				String port = portIt.next();
				if (!(port.equalsIgnoreCase(myPort))) {
					Message msg = new Message();
					msg.setPort(myPort);
					msg.setKey("@");
					msg.setType(2);
					// Send Data to Port
					synchronized(SimpleDynamoProvider.waitObj){
						deleteLock.lock();
						new Thread(new ClientTask(listMap.get(port), msg)).start();

						try {
							deleteCondition.await();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						deleteLock.unlock();
					}

					// Wait for 100 ms
				}
			}
			
		} else {
			if(uri.toString().equalsIgnoreCase(dummyUri.toString())){
				sqliteDatabase = dbHelper.getWritableDatabase();
				Log.v("Query Delete", selection);
				sqliteDatabase.delete(MessengerDBHelper.TABLENAME,conditionSb.toString(), args);
				List<String> nodeList = getNodeList(hash,selection);
				if(nodeList.get(0).equalsIgnoreCase(myPort)){
					Message msg = new Message();
					msg.setPort(myPort);
					msg.setKey(selection);
					msg.setType(2);

					nodeList.remove(0);
					
					Iterator<String> portIt = nodeList.iterator();
					while (portIt.hasNext()) {
						String port = portIt.next();
						synchronized (SimpleDynamoProvider.waitObj) {
							deleteLock.lock();
							new Thread(new ClientTask(port, msg)).start();
							Log.v("Delete","Holding Delete Wait"+port+"::key::"+msg.key);
							try {
								deleteCondition.await(1L,TimeUnit.SECONDS);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							deleteLock.unlock();
						}
					}
				}
			} else {
				
				List<String> nodeList = getNodeList(hash,selection);
				args[0] = selection;
				Iterator<String> portIt = nodeList.iterator();
				boolean notify = false;
				String partition = nodeList.get(0);
				while (portIt.hasNext() && !notify) {
					String port = portIt.next();
					Message msg = new Message();
					msg.setPort(myPort);
					msg.setKey(selection);
					msg.setType(2);
					// Send Data to Port
					// Wait for reply
					synchronized (SimpleDynamoProvider.waitObj) {
						deleteLock.lock();
						new Thread(new ClientTask(port, msg)).start();
						try {
							notify = deleteCondition.await(1L,TimeUnit.SECONDS);
							
							if(!(port.equalsIgnoreCase(partition))){
								notify = false;
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						deleteLock.unlock();
					}
				}
			}
		}
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// You do not need to implement this.
		return 0;
	}

	private String genHash(String input) throws NoSuchAlgorithmException {
		MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
		byte[] sha1Hash = sha1.digest(input.getBytes());
		Formatter formatter = new Formatter();
		for (byte b : sha1Hash) {
			formatter.format("%02x", b);
		}
		return formatter.toString();
	}
}