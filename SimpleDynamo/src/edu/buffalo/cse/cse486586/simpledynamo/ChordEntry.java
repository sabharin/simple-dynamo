package edu.buffalo.cse.cse486586.simpledynamo;

public interface ChordEntry extends Comparable<ChordEntry>{
	public String getHash();

	int compareTo(ChordEntry another);
}
